package edu.coursera.parallel;

import junit.framework.TestCase;

import static edu.coursera.parallel.ReciprocalArraySum.parManyTaskArraySum;

public class ReciprocalTestPack2 extends TestCase {

    private void testN(int n) {
        double[] input = new double[n];
        for(int i=0; i<n;i++){
            input[i]=i+1;
        }
        double expectedSum = reciprocalArraySum(input);
        long before = System.currentTimeMillis();
        assertEquals(expectedSum, parManyTaskArraySum(input, Runtime.getRuntime().availableProcessors()), 0.1);
        System.out.printf("Recursive parManyTaskArraySum took %d milliseconds to complete for %d elements\n", System.currentTimeMillis() - before, input.length);
    }

    private double reciprocalArraySum(double[] input) {
        long before = System.currentTimeMillis();
        double sum = 0.0;
        for (int i = 0; i < input.length; i++) {
            sum += 1/input[i];
        }
        System.out.printf("Sequential reciprocalArraySum took %d milliseconds to complete for %d elements\n", System.currentTimeMillis() - before, input.length);
        return sum;
    }


    public void test0() {
        testN(0);
    }

    public void test2() {
        testN(2);
    }

    public void test300() {
        testN(300);
    }

    public void testMillion() {
        testN(1_000_001);
    }

    public void test200Million() {
        testN(200_000_000);
    }

}
