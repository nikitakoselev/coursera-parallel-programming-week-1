package edu.coursera.parallel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.stream.Collectors;

/**
 * Class wrapping methods for implementing reciprocal array sum in parallel.
 */
public final class ReciprocalArraySum {

    public static final int MINIMAL_THRESHOLD = 50000;

    /**
     * Default constructor.
     */
    private ReciprocalArraySum() {
    }

    /**
     * Sequentially compute the sum of the reciprocal values for a given array.
     *
     * @param input Input array
     * @return The sum of the reciprocals of the array input
     */
    protected static double seqArraySum(final double[] input) {
        double sum = 0;

        // Compute sum of reciprocals of array elements
        for (int i = 0; i < input.length; i++) {
            sum += 1 / input[i];
        }

        return sum;
    }

    /**
     * Computes the size of each chunk, given the number of chunks to create
     * across a given number of elements.
     *
     * @param nChunks   The number of chunks to create
     * @param nElements The number of elements to chunk across
     * @return The default chunk size
     */
    private static int getChunkSize(final int nChunks, final int nElements) {
        // Integer ceil
        return (nElements + nChunks - 1) / nChunks;
    }

    /**
     * Computes the inclusive element index that the provided chunk starts at,
     * given there are a certain number of chunks.
     *
     * @param chunk     The chunk to compute the start of
     * @param nChunks   The number of chunks created
     * @param nElements The number of elements to chunk across
     * @return The inclusive index that this chunk starts at in the set of
     * nElements
     */
    private static int getChunkStartInclusive(final int chunk,
                                              final int nChunks, final int nElements) {
        final int chunkSize = getChunkSize(nChunks, nElements);
        return chunk * chunkSize;
    }

    /**
     * Computes the exclusive element index that the provided chunk ends at,
     * given there are a certain number of chunks.
     *
     * @param chunk     The chunk to compute the end of
     * @param nChunks   The number of chunks created
     * @param nElements The number of elements to chunk across
     * @return The exclusive end index for this chunk
     */
    private static int getChunkEndExclusive(final int chunk, final int nChunks,
                                            final int nElements) {
        final int chunkSize = getChunkSize(nChunks, nElements);
        final int end = (chunk + 1) * chunkSize;
        if (end > nElements) {
            return nElements;
        } else {
            return end;
        }
    }

    /**
     * This class stub can be filled in to implement the body of each task
     * created to perform reciprocal array sum in parallel.
     */
    private static class ReciprocalArraySumTask extends RecursiveAction {
        /**
         * Starting index for traversal done by this task.
         */
        private final int startIndexInclusive;
        /**
         * Ending index for traversal done by this task.
         */
        private final int endIndexExclusive;
        /**
         * Input array to reciprocal sum.
         */
        private final double[] input;


        private final int numTasks;
        private final int threshold;

        /**
         * Intermediate value produced by this task.
         */
        private double value;


        /**
         * Constructor.
         *
         * @param threshold
         * @param startIndexInclusive Set the starting index to begin
         *                            parallel traversal at.
         * @param endIndexExclusive   Set ending index for parallel traversal.
         * @param input               Input values
         * @param numTasks
         */
        ReciprocalArraySumTask(int threshold, final int startIndexInclusive,
                               final int endIndexExclusive, final double[] input, int numTasks) {
//            System.out.printf("ReciprocalArraySumTask start:%d, end:%d, input size:%d, numTasks:%d\n",
//                    startIndexInclusive, endIndexExclusive, input.length, numTasks);
            this.startIndexInclusive = startIndexInclusive;
            this.endIndexExclusive = endIndexExclusive;
            this.input = input;
            this.numTasks = numTasks;
            this.threshold = threshold;
        }

        /**
         * Getter for the value produced by this task.
         *
         * @return Value produced by this task
         */
        public double getValue() {
            return value;
        }

        @Override
        protected void compute() {
            if ((endIndexExclusive - startIndexInclusive) <= threshold) {
                for (int i = startIndexInclusive; i < endIndexExclusive; i++) {
                    this.value += 1 / input[i];
                }
            } else {
                List<ReciprocalArraySumTask> tasks = new ArrayList<>();
                for (int i = 0; i < numTasks; i++) {
                    int chunkSize = getChunkSize(numTasks, endIndexExclusive - startIndexInclusive);
                    tasks.add(new ReciprocalArraySumTask(threshold, startIndexInclusive + getChunkStartInclusive(i, numTasks, endIndexExclusive - startIndexInclusive),
                            startIndexInclusive + getChunkEndExclusive(i, numTasks, endIndexExclusive - startIndexInclusive),
                            input,
                            numTasks));
                }
                for (int i = 0; i < (numTasks - 1); i++) {
                    tasks.get(i).fork();
                }
                tasks.get(numTasks - 1).compute();

                for (int i = 0; i < (numTasks - 1); i++) {
                    tasks.get(i).join();
                }
                this.value = tasks.stream().collect(Collectors.summingDouble(i -> i.value));
            }
        }
    }

    /**
     * @param input Input array
     * @return The sum of the reciprocals of the array input
     */
    protected static double parArraySum(final double[] input) {
        return parManyTaskArraySum(input, 2);
    }

    /**
     * @param input    Input array
     * @param numTasks The number of tasks to create
     * @return The sum of the reciprocals of the array input
     */
    protected static double parManyTaskArraySum(final double[] input,
                                                final int numTasks) {
        ReciprocalArraySumTask rast = new ReciprocalArraySumTask(input.length <= MINIMAL_THRESHOLD ? input.length : (int) (Math.log(input.length) / Math.log(numTasks)), 0, input.length, input, numTasks);
        ForkJoinPool.commonPool().invoke(rast);
        return rast.value;
    }
}
